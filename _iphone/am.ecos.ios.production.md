---
wsId: ECOS
title: 'ECOS: BTC mining,crypto wallet'
altTitle: 
authors:
- danny
appId: am.ecos.ios.production
appCountry: us
idd: 1528964374
released: 2020-11-25
updated: 2022-04-08
version: 1.23.4
stars: 3.9
reviews: 61
size: '64301056'
website: https://ecos.am/
repository: 
issue: 
icon: am.ecos.ios.production.jpg
bugbounty: 
meta: ok
verdict: custodial
date: 2021-10-07
signer: 
reviewArchive: 
twitter: ecosmining
social:
- https://www.facebook.com/ecosdefi

---

{% include copyFromAndroid.html %}
