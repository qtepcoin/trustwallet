---
wsId: bitmart
title: 'BitMart: Trade BTC, ETH, SHIB'
altTitle: 
authors:
- leo
appId: com.bitmart.exchange
appCountry: 
idd: 1396382871
released: 2018-08-02
updated: 2022-04-09
version: 2.9.11
stars: 3.5
reviews: 3946
size: '115927040'
website: https://www.bitmart.com/
repository: 
issue: 
icon: com.bitmart.exchange.jpg
bugbounty: 
meta: ok
verdict: custodial
date: 2021-05-20
signer: 
reviewArchive: 
twitter: BitMartExchange
social:
- https://www.linkedin.com/company/bitmart
- https://www.facebook.com/bitmartexchange
- https://www.reddit.com/r/BitMartExchange

---

On their website we read:

> **Secure**<br>
  Advanced risk control system in the market. Hybrid hot/cold wallet systems and
  multi-signature technologies. 100% secure for trading and digital asset
  management

A "hot" wallet is online, a "cold" wallet is offline. Your phone is certainly
not "cold", so it's them who hold the keys. As a custodial service the app is
**not verifiable**.
