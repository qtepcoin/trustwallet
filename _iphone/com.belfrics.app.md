---
wsId: Belfrics
title: Belfrics Exchange
altTitle: 
authors:
- danny
appId: com.belfrics.app
appCountry: us
idd: 1299601017
released: 2017-12-23
updated: 2022-04-03
version: 2.5.12
stars: 4.9
reviews: 26
size: '60121088'
website: https://www.belfrics.io/
repository: 
issue: 
icon: com.belfrics.app.jpg
bugbounty: 
meta: ok
verdict: custodial
date: 2021-11-15
signer: 
reviewArchive: 
twitter: Belfrics
social:
- https://www.facebook.com/Belfrics

---

{% include copyFromAndroid.html %}
