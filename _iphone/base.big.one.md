---
wsId: BigONE
title: BigONE
altTitle: 
authors: 
appId: base.big.one
appCountry: us
idd: 1485385044
released: 2019-11-06
updated: 2022-04-04
version: 2.2.222
stars: 4.5
reviews: 106
size: '176570368'
website: https://big.one
repository: 
issue: 
icon: base.big.one.jpg
bugbounty: 
meta: ok
verdict: custodial
date: 2021-09-17
signer: 
reviewArchive: 
twitter: BigONEexchange
social:
- https://www.facebook.com/exBigONE
- https://www.reddit.com/r/BigONEExchange

---

 {% include copyFromAndroid.html %}
