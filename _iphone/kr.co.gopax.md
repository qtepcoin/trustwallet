---
wsId: gopax
title: GOPAX
altTitle: 
authors:
- danny
appId: kr.co.gopax
appCountry: kr
idd: 1369896843
released: 2018-06-21
updated: 2022-04-11
version: 1.8.6
stars: 2.8
reviews: 440
size: '53662720'
website: https://www.gopax.co.kr/notice
repository: 
issue: 
icon: kr.co.gopax.jpg
bugbounty: 
meta: ok
verdict: custodial
date: 2021-10-01
signer: 
reviewArchive: 
twitter: 
social: 

---

 {% include copyFromAndroid.html %}
