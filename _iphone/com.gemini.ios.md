---
wsId: geminiwallet
title: 'Gemini: Buy Bitcoin & Crypto'
altTitle: 
authors:
- leo
appId: com.gemini.ios
appCountry: 
idd: 1408914447
released: 2018-12-11
updated: 2022-04-06
version: 22.405.0
stars: 4.8
reviews: 85902
size: '86390784'
website: http://gemini.com
repository: 
issue: 
icon: com.gemini.ios.jpg
bugbounty: 
meta: ok
verdict: custodial
date: 2021-01-04
signer: 
reviewArchive: 
twitter: gemini
social:
- https://www.linkedin.com/company/geminitrust
- https://www.facebook.com/GeminiTrust

---

This provider being an exchange, together with the lack of clear words of who
gets to hold the private keys leads us to believe this app is only an interface
to the Gemini exchange account and thus custodial and thus **not verifiable**.
