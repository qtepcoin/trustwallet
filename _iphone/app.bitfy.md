---
wsId: bitfy
title: Bitfy
altTitle: 
authors:
- danny
appId: app.bitfy
appCountry: us
idd: 1483269793
released: 2019-11-26
updated: 2022-04-07
version: 3.10.29
stars: 3
reviews: 4
size: '27880448'
website: https://bitfy.app
repository: 
issue: 
icon: app.bitfy.jpg
bugbounty: 
meta: ok
verdict: nosource
date: 2021-09-03
signer: 
reviewArchive: 
twitter: bitfyapp
social:
- https://www.facebook.com/bitfyapp

---

{% include copyFromAndroid.html %}
