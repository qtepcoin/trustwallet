---
wsId: changeinvest
title: 'Change: Simple BTC Investing'
altTitle: 
authors:
- danny
appId: com.getchange.dev
appCountry: nl
idd: 1442085358
released: 2018-11-15
updated: 2022-04-11
version: 20.11.1
stars: 4.1
reviews: 31
size: '86952960'
website: https://www.changeinvest.com/
repository: 
issue: 
icon: com.getchange.dev.jpg
bugbounty: 
meta: ok
verdict: custodial
date: 2022-01-04
signer: 
reviewArchive: 
twitter: changefinance
social:
- https://www.linkedin.com/company/changeinvest
- https://www.facebook.com/changeinvest

---

{% include copyFromAndroid.html %}
