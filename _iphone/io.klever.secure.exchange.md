---
wsId: kleverexchange
title: Klever Exchange
altTitle: 
authors:
- danny
appId: io.klever.secure.exchange
appCountry: us
idd: 1553486059
released: 2021-09-25
updated: 2022-04-05
version: 1.1.0
stars: 4.8
reviews: 44
size: '105395200'
website: https://klever.io
repository: 
issue: 
icon: io.klever.secure.exchange.jpg
bugbounty: 
meta: ok
verdict: custodial
date: 2021-10-05
signer: 
reviewArchive: 
twitter: klever_io
social:
- https://www.linkedin.com/company/klever-app
- https://www.facebook.com/klever.io

---

{% include copyFromAndroid.html %}
