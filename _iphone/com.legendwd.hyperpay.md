---
wsId: hyperPay
title: 'HyperPay: Crypto & BTC Wallet'
altTitle: 
authors:
- danny
appId: com.legendwd.hyperpay
appCountry: us
idd: 1354755812
released: 2018-09-29
updated: 2022-04-08
version: 4.1.31
stars: 3.8
reviews: 88
size: '298730496'
website: https://hyperpay.tech
repository: 
issue: 
icon: com.legendwd.hyperpay.jpg
bugbounty: 
meta: ok
verdict: obfuscated
date: 2022-01-10
signer: 
reviewArchive: 
twitter: HyperPay_tech
social:
- https://www.facebook.com/hyperpayofficial

---

{% include copyFromAndroid.html %}