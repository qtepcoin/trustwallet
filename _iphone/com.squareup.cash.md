---
wsId: CashApp
title: Cash App
altTitle: 
authors:
- leo
appId: com.squareup.cash
appCountry: 
idd: 711923939
released: 2013-10-16
updated: 2022-04-10
version: 3.62.2
stars: 4.7
reviews: 2062827
size: '260961280'
website: https://cash.app
repository: 
issue: 
icon: com.squareup.cash.jpg
bugbounty: 
meta: ok
verdict: custodial
date: 2020-12-22
signer: 
reviewArchive: 
twitter: cashapp
social: 

---

On their website the provider claims:

> **Coin Storage**<br>
  Your Bitcoin balance is securely stored in our offline system

which means it is custodial.
