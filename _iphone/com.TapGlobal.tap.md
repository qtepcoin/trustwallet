---
wsId: tapngo
title: Tap - Buy & Sell Bitcoin
altTitle: 
authors:
- danny
appId: com.TapGlobal.tap
appCountry: gb
idd: 1492263993
released: 2019-12-20
updated: 2022-04-08
version: 2.3.6
stars: 4.7
reviews: 651
size: '190360576'
website: https://www.tap.global/
repository: 
issue: 
icon: com.TapGlobal.tap.jpg
bugbounty: 
meta: ok
verdict: custodial
date: 2021-09-15
signer: 
reviewArchive: 
twitter: 
social: 

---

 {% include copyFromAndroid.html %}
