---
wsId: crypterium
title: Crypterium | Bitcoin Wallet
altTitle: 
authors:
- leo
appId: com.Crypterium.Crypterium
appCountry: 
idd: 1360632912
released: 2018-03-26
updated: 2022-04-08
version: 1.19.5
stars: 4.3
reviews: 939
size: '275817472'
website: https://cards.crypterium.com/visa
repository: 
issue: 
icon: com.Crypterium.Crypterium.jpg
bugbounty: 
meta: ok
verdict: custodial
date: 2021-05-20
signer: 
reviewArchive: 
twitter: crypterium
social:
- https://www.facebook.com/crypterium.org

---

Judging by what we can find on the [wallet site](https://wallet.crypterium.com/):

> **Store**<br>
  keep your currencies<br>
  safe & fully insured

this is a custodial app as a self-custody wallet cannot ever have funds insured.

As a custodial app it is **not verifiable**.
